﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace COREAPI2.Model
{
    public class Grant
    {
        [Key]
        public int GRANT_ID { get; set; }

        public string GRANT_AGENCYNAME { get; set; }
        public string GRANT_DEPARTMENT { get; set; }
        public string GRANT_POINTOFCONTACT { get; set; }
        public string GRANT_ADDRESS { get; set; }
        public string GRANT_PHONENUMBER { get; set; }
        public string GRANT_FAXNUMBER { get; set; }
        public string GRANT_EMAILADDRESS { get; set; }
        public string GRANT_DUNS { get; set; }
    }
}
