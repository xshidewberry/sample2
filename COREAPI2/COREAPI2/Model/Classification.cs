﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace COREAPI2.Model
{
    public class Classification
    {
        [Key]
        public Int16 CLASSIFICATION_ID { get; set; }

        public string CLASSIFICATION_NAME { get; set; }
        public Int16 CLASSIFICATION_SORTORDER { get; set; }
        public Int16? SECTION_ID { get; set; }
        public int? ACTIVE { get; set; }
    }
}
